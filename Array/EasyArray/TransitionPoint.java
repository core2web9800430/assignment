  /*Given a sorted array containing only 0s and 1s, find the transition point.
   * Example 1:
   * Input:N = 5
   * arr[] = {0,0,0,1,1}
   * Output: 3
   * Explanation: index 3 is the transition point where 1 begins.
   * Example 2:
   * Input:N = 4
   * arr[] = {0,0,0,0}
   * Output: -1
   * Explanation: Since, there is no "1", the answer is -1.
   */ 
  import java.io.*;
  import java.util.*;
  class TransitionPoint {
	  public static void main(String [] args) throws IOException{
		  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		  System.out.println(" Enter the size of an array:");
		  int size = Integer.parseInt(br.readLine());
		  int arr[] = new int[size];
		  System.out.println("Enter the Array Elements in Array:");
		  for(int i=0; i<size; i++){
			  arr[i]= Integer.parseInt(br.readLine());
		  }
		  Arrays.sort(arr);
		  boolean check=true;
		  for(int i=0; i<size-1; i++){
			  if(arr[i]!=arr[i+1]){
			  check = false;
			  System.out.println("Transition  point from Index:"+(i+1));
			  }
		  } 
		  if(check){
			  System.out.println("-1  \'No Transition point\' ");
		  }
	  }
  }
