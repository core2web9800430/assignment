/*
 * Given an array of size N containing only 0s, 1s, and 2s; sort the array in ascending order.
 * Example 1:
 * Input:
 * N = 5
 * arr[]= {0 2 1 2 0}
 * Output:
 * 0 0 1 2 2
 * Explanation: 0s 1s and 2s are segregated into ascending order.
 * Example 2:
 * Input:
 * N = 3
 * arr[] = {0 1 0}
 * Output:
 * 0 0 1
 * Explanation: 0s 1s and 2s are segregated into ascending order
 */
   
  import java.io.*;
  import java.util.*;
  class SortAscending {
	  public static void main(String [] args) throws IOException{
		  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		  System.out.println(" Enter the size of an array:");
		  int size = Integer.parseInt(br.readLine());
		  int arr[] = new int[size];
		  System.out.println("Enter the Array Elements in Array:");
		  for(int i=0; i<size; i++){
			  arr[i]= Integer.parseInt(br.readLine());
		  }
		  Arrays.sort(arr);
		  System.out.print("The Sorted Array is:");
		  for(int i=0; i<size; i++){
			  System.out.print(arr[i]);
		  }
	  }
  }
