/*Given an array of N integers, and an integer K, find the number of pairs of elements in the array whose sum is equal to K.
 * Example 1:
 * Input:N = 4, K = 6
 * arr[] = {1, 5, 7, 1}
 * Output: 2
 * Explanation:
 * arr[0] + arr[1] = 1 + 5 = 6
 * and arr[1] + arr[3] = 5 + 1 = 6.
 * Example 2:
 * Input: N = 4, K = 2
 * arr[] = {1, 1, 1, 1}
 * Output: 6
 * Explanation:
 * Each 1 will produce sum 2 with any 1.
 */ 
 import java.io.*;
 class  CountPairWithGivenSum {
	static int pair(int arr[],int n ,int k ){
		int count = 0;
		for(int i=0; i<n; i++){ 
			int sum=0;
			for(int j=i+1;j<n;j++){
			     sum=arr[i]+arr[j];
			     if(sum==k){
				     count++;
			}
	       	} 
       	}
	if(count>0){
		return count;
		} 	
	return -1;	    	
	}
                               
	 public static void main(String [] args) throws IOException{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter the Size of an Array:");
		 int n = Integer.parseInt(br.readLine());
		 System.out.println("Enter the Elements in array:");
		 int arr[]= new int[n];
		 for(int i=0; i<n; i++){
			 arr[i] = Integer.parseInt(br.readLine());
		 } 
		 System.out.println("Enter the value of k:");
                 int k = Integer.parseInt(br.readLine());
		 int num = pair(arr,n,k);
		 if(num==-1){
                      System.out.println("No Pair sum is equal of "+k);
		 } else {
			 System.out.println(num+" pair of sum is equal of "+k);
		 }
	 }
 }

