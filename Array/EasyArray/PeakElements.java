/*An element is called a peak element if its value is not smaller than the value of its adjacent elements(if they exist).
 * Given an array arr[] of size N, Return the index of any one of its peak elements.
 * Note: The generated output will always be 1 if the index that you return is correct.Otherwise output will be 0.
 * Example 1:
 * Input:N = 3
 * arr[] = {1,2,3}
 * Possible Answer: 2
 * Generated Output: 1
 * Explanation: index 2 is 3. It is the peak element as it is greater than its neighbor
 * 2. If 2 is returned then the generated output will be 1 else 0.
 * Example 2:
 * Input:N = 3
 * arr[] = {3,4,2}
 * Possible Answer: 1
 * Output: 1
 * Explanation: 4 (at index 1) is the peak element as it is greater than it's neighbor
 * elements 3 and 2. If 1 is returned then the generated output will be 1 else 0.
 */
  import java.io.*;
  class PeakElements {
	 static void peak(int arr[],int n){
		 int count=0;
		 if (arr[n - 1] >= arr[n - 2])
			             count++;
		 for(int i =1; i<n-1;i++){
			if(arr[i-1] <arr[i] && arr[i]>arr[i+1]){
			      count++;
			}
		 } if(count>0) {
		System.out.println(count+ " peak elements present in array");
		 } else {
			System.out.println("0 peak elements present in array");
		 } 	 
	 }

   public static void main(String [] args) throws IOException{
   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
   System.out.println("Enter the size of array:");
   int n = Integer.parseInt(br.readLine());
   System.out.println("Enter the elements in array");
   int arr[] =new int[n];
   for( int i=0; i<n; i++){
	   arr[i]=Integer.parseInt(br.readLine());
   } 
    peak(arr,n);
   } 
  }
