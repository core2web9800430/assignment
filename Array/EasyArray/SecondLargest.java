/*Given an array Arr of size N, print the second largest distinct element from an array.
 * Example 1:
 * Input: N = 6
 * Arr[] = {12, 35, 1, 10, 34, 1}
 * Output: 34
 * Explanation: The largest element of the array is 35 and the second largest element
 is 34.
 * Example 2:
 * Input:N = 3
 * Arr[] = {10, 5, 10}
 * Output: 5
 * Explanation: The largest element of the array is 10 and the second largest element is 5.
 */
   import java.io.*;
   import java.util.*;
   class SecondLargest {
	   public static void main(String [] args) throws IOException{
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println("Enter the size of array:");
		   int n=Integer.parseInt(br.readLine());
		   System.out.println("Enter the Elements of Array:");
		   int arr[] = new int[n];
		   for(int i=0; i<n; i++){
			   arr[i]=Integer.parseInt(br.readLine());
		   }
		   Arrays.sort(arr);
		  <F3> if(arr[n-1]==arr[n-2]){
			   System.out.println("Second largest is: "+arr[n-3]);
		   } else {
			   System.out.println("Second largest is: "+arr[n-2]);
		   }
	   }
   }













