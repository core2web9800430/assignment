/*
 * Given an array of size N-1 such that it only contains distinct integers in the range of 1 to
 * N. Find the missing element.
 * Example 1:
 * Input:
 * N = 6
 * A[] = {1,2,4,5,6}
 * Output: 3
 * Example 2:
 * Input:
 * N = 11
 * A[] = {1,3,2,5,6,7,8,11,10,4}
 * Output: 9
 */
 
 import java.io.*;
 class MissingNumber {
	 public static void main(String [] args) throws IOException{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter the size of Array");
		 int size = Integer.parseInt(br.readLine());
		 int arr[]= new int[size-1];
		 System.out.println("Enter the elements of array");
		 for(int i=0; i<arr.length; i++){
			 arr[i]= Integer.parseInt(br.readLine());
		  } 
		 for (int i=0; i<size;i++){
			 if(arr[i]!=(i+1)){
				 System.out.println("missing element is: "+(i+1));
				 break;
			 } 
		 }
	 }
 }

