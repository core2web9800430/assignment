  /*Given an array arr[] of size n, find the first repeating element. The element should occur
   * more than once and the index of its first occurrence should be the smallest.
   * Note:- The position you return should be according to 1-based indexing.
   * Example 1:
   * Input:n = 7
   * arr[] = {1, 5, 3, 4, 3, 5, 6}
   * Output: 2
   * Explanation: 5 is appearing twice and its first appearance is at index 2 which is
   * less than 3 whose first occurring index is 3.
   * Example 2:
   * Input:n = 4
   * arr[] = {1, 2, 3, 4}
   * Output: -1
   * Explanation: All elements appear only once so the answer is -1.
   */

  import java.io.*;
  import java.util.*;
  class  FirstRepeatingElement {
	  public static void main(String [] args) throws IOException{
		  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		  System.out.println(" Enter the size of an array:");
		  int size = Integer.parseInt(br.readLine());
		  int arr[] = new int[size];
		  System.out.println("Enter the Array Elements in Array:");
		  for(int i=0; i<size; i++){
			  arr[i]= Integer.parseInt(br.readLine());
		  }
		  boolean check=true;
		  for(int i=0; i<size; i++){
			  for(int j=i+1;j<size; j++){
			  if(arr[i]==arr[j]){
			  check = false;
			  System.out.println("First repeating element at index: "+(i+1));
			  break;
			  }
		  } if(!check)
		 break; } 
		  if(check){
			  System.out.println("-1  \'No Repeating Element\' ");
		  }
	  }
  } 
  
