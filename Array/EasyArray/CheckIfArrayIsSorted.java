/*Given an array arr[] of size N, check if it is sorted in non-decreasing order or not.
 * Example 1:
 * Input:N = 5
 * arr[] = {10, 20, 30, 40, 50}
 * Output: 1
 * Explanation: The given array is sorted.
 * Example 2:
 * Input:N = 6
 * arr[] = {90, 80, 100, 70, 40, 30}
 * Output: 0
 * Explanation: The given array is not sorted.
 */   

   import java.io.*;
   import java.util.*;
   class CheckIfArrayIsSorted {
	   public static void main(String [] args) throws IOException{
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println("Enter the size of array:");
		   int n0 =Integer.parseInt(br.readLine());
		   System.out.println("Enter the Elements of Array:");
		   int arr[] = new int[n0];
		   int arr2[] = new int[n0];
		   for(int i=0; i<n0; i++){
			   arr[i]=Integer.parseInt(br.readLine());
		   } 
		   arr2=Arrays.copyOf(arr,n0);
		    Arrays.sort(arr2);
		   boolean check= true;
		   for(int i=0; i<n0; i++){
			   if(arr[i]!=arr2[i]){
			          check=false;
			   }
		   } 
		   if(check){
			   System.out.println("Array is Sorted ");
		   } else {
			   System.out.println("not Sorted ");
		   }
	   }
   }
   













