/*Given an array of size N which contains elements from 0 to N-1, you need to find all the
 * elements occurring more than once in the given array. Return the answer in ascending
 * order. If no such element is found, return list containing [-1].
 * Note: The extra space is only for the array to be returned. Try and perform all operations
 * within the provided array.
 * Example 1:
 * Input:N = 4
 * a[] = {0,3,1,2}
 * Output:-1
 *  Explanation:
 *  There is no repeating element in the array. Therefore output is -1.
 *  Example 2:
 *  Input:N = 5
 *  a[] = {2,3,1,2,3}
 *  Output: 2 3
 *  Explanation:
 *  2 and 3 occur more than once in the given array.
 */
 import java.io.*;
 import java.util.*;
 class Duplicates {
	static void dupli(int arr[],int n){
		Arrays.sort(arr);
		boolean check = true;
		for(int i=0; i<n; i++){
			for(int j=i+1;j<n;j++){
				if(arr[i]==arr[j]){
					check=false;
					System.out.print(arr[i] + " ");
				}
			}
		} if(check){
			System.out.println("-1 \'No repeating elements in Array\'");
		}
	}

                                 
	 public static void main(String [] args) throws IOException{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		 System.out.println("Enter the Size of an Array:");
		 int n = Integer.parseInt(br.readLine());
		 System.out.println("Enter the Elements in array:");
		 int arr[]= new int[n];
		 for(int i=0; i<n; i++){
			 arr[i] = Integer.parseInt(br.readLine());
		 } 
		 dupli(arr,n);
	 }
 }

