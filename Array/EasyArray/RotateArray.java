/*Given an unsorted array arr[] of size N. Rotate the array to the left (counter-clockwise direction) by D steps, where D is a positive integer.
 * Example 1:
 * Input: N = 5, D = 2
 * arr[] = {1,2,3,4,5}
 * Output: 3 4 5 1 2
 * Explanation: 1 2 3 4 5 when rotated
 * by 2 elements, it becomes 3 4 5 1 2.
 * Example 2:
 * Input:N = 10, D = 3
 * arr[] = {2,4,6,8,10,12,14,16,18,20}
 * Output: 8 10 12 14 16 18 20 2 4 6
 * Explanation: 2 4 6 8 10 12 14 16 18 20
 * when rotated by 3 elements, it becomes
 * 8 10 12 14 16 18 20 2 4 6.
 */
   import java.io.*;
   import java.util.*;
   class CheckIfArrayIsSorted {
	   public static void main(String [] args) throws IOException{
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println("Enter the size of array:");
		   int n0 =Integer.parseInt(br.readLine());
		   System.out.println("Enter the Elements of Array:");
		   int arr[] = new int[n0];
		   int arr2[] = new int[n0];
		   for(int i=0; i<n0; i++){
			   arr[i]=Integer.parseInt(br.readLine());
		   } 
		   System.out.println("Enter the number by rotate Array:");
		   int d= Integer.parseInt(br.readLine());
		   int j =0;
		   for(int i=d; i<n0; i++){
			   arr2[j]=arr[i];
			         j++;
			   }
		    for(int i=0; i<d; i++){
			   arr2[j] = arr[i];
			   j++;}
		   for(int num :arr2){
			   System.out.print(num+" ");
		   }
	   }
   }
   

   













