/*
 Write a program to create an array of ‘n’ integer elements.
 Where ‘n’ value should be taken from the user.
 Insert the values from the user and find the frequency of digit
Input:
n=5
Enter elements in the array:
2
3
6
3
5
2
Output:
frequency of 2 is 2
frequency of 3 is 2
frequency of 6 is 1
frequency of 5 is 1
 */
   
   import java.io.*;
   class Program8 {
	   public static void main(String [] args ) throws IOException{
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println("Enter the Number of Elements:");
		   int n = Integer.parseInt(br.readLine());
		   int arr[] = new int[n];
		   System.out.println("Enter the Elements:");
		   for(int i=0; i<arr.length;i++){
			   arr[i] = Integer.parseInt(br.readLine());               
       		   } 
		   int arr2[] = new int[n];
		   for (int i=0; i<arr.length; i++){
			  int count=0;
			   for(int j=0; j<arr.length; j++){
				   if(arr[i]==arr[j]){
				count++;
				   }
			   } 
			   System.out.println("Frequency of "+arr[i]+" is : "+count);
				   }
			   }
	   }
   
   

 
