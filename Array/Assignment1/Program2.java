// Write a program to create an array of 5 integer element 
// insert from the user and print 5 elements from an array
// Input :1
//        2
//        3
//        4
//        5
// Output :1
//         2
//         3
//         4
//         5
     
    import java.io.*;
    class Program2 {
	    public static void main(String [] args)throws IOException{
		    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		    System.out.println("Enter the five integer elements: ");
		    int arr[] = new int[5];
		    for(int i=0; i<arr.length; i++){
			    arr[i] = Integer.parseInt(br.readLine());
		    }
		    System.out.println("Enteger are: "); 
		    for(int i=0; i<arr.length; i++){
			    System.out.println(arr[i]);
		    }
	    }
    }

