import java.util.Scanner;

public class Main {
	    public static void main(String[] args) {
		            Scanner scanner = new Scanner(System.in);

			            System.out.print("Enter the number of elements in the array: ");
				            int n = scanner.nextInt();

					            int[] arr = new int[n];

						            System.out.println("Enter elements in the array:");
							            for (int i = 0; i < n; i++) {
									                arr[i] = scanner.nextInt();
											        }

								            int[] digitFrequency = new int[10]; // Initialize an array to store digit frequencies (0-9)

									            for (int num : arr) {
											                while (num != 0) {
														                int digit = num % 10;
																                digitFrequency[digit]++;
																		                num /= 10;
																				            }
													        }

										            for (int i = 0; i < 10; i++) {
												                if (digitFrequency[i] > 0) {
															                System.out.println("Frequency of " + i + " is " + digitFrequency[i]);
																	            }
														        }

											            scanner.close();
												        }
}

