/*
 * Given an integer array and another integer element. The task is to find if the given
 * element is present in the array or not.
 * Example 1:
 * Input:
 * n = 4
 * arr[] = {1,2,3,4}
 * x = 3
 * Output: 2
 * Explanation: There is one test case with an array as {1, 2, 3 4} and an
 * element to be searched as 3. Since 3 is present at index 2, output is 2.
 */

import java.io.*;
class Program1 {
	public static void main(String [] args ) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter the Elements in array:");
		for (int i=0; i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		System.out.println("Enter the Number to check:");
		int check=Integer.parseInt(br.readLine());
		for (int i=0; i<size; i++){
			if(arr[i]==check){
				System.out.println( "the Given Number "+check+" is found at index: "+i);
			}
		}
	}
}

