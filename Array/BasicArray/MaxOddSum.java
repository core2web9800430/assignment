/*
 * Given an array of integers, check whether there is a subsequence with odd sum and
 * if yes, then find the maximum odd sum. If no subsequence contains an odd sum,
 * print -1.
 * Example 1:
 * Input:
 * N=4
 * arr[] = {4, -3, 3, -5}
 * Output: 7
 * Explanation:
 * The subsequence with maximum odd
 * sum is 4 + 3 = 7
 * Example 2:
 * Input:
 * N=5
 * arr[] = {2, 5, -4, 3, -1}
 * Output: 9
 * Explanation
 */
  
   import java.util.*;
   class MaxOddSum {
	   public static void main(String [] args){
		   Scanner sc= new Scanner(System.in);
		   System.out.println("The size of Array");
		   int size = sc.nextInt();
		   int arr[] = new int[size];
		   int count=0;
		   int min=Integer.MAX_VALUE;
		   int sum=0;
		   System.out.println("Enter the array elements:");
		   for (int i=0; i<size; i++){
			   arr[i]=sc.nextInt();
			   if (min > Math.abs(arr[i])){
		               min = Math.abs(arr[i]);
			           }
			   if(arr[i]<0 || arr[i]%2==0){
				   count++;
			   } 
			    if(arr[i]>0){
				    sum+=arr[i];                                                                                                                                                      }
		   } 
		   if(count == size){
			   System.out.println("-1 \'No Subsequence contains an odd sum\'");
		   } else {
			   if(sum%2 == 0){
	    			sum -= min;
				System.out.println("The Maximum odd sum is : "+sum);
			   } else {
				  System.out.println("The Maximum odd sum is : "+sum);
			   } 
		   } 
	   }
   } 
			   
