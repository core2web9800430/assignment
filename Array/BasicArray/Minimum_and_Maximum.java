/*
 * Given an array A of size N of integers. Your task is to find the minimum and
 * maximum elements in the array.
 * Example 1:
 * Input:
 * N = 6
 * A[] = {3, 2, 1, 56, 10000, 167}
 * Output: 1 10000
 * Explanation: minimum and maximum elements of array are 1 and 10000.
 */
    import java.io.*;
    class Minimum_and_Maximum {
	    public static void main(String[]args)throws IOException{
		    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		    System.out.println("Enter the size of an array:");
		    int size =Integer.parseInt(br.readLine());
		    System.out.println("Enter the Elements in Array:");
		    int arr[] = new int[size];
		    int max=0;
		    int min=0;
		    for(int i=0; i<arr.length; i++){
			    arr[i]=Integer.parseInt(br.readLine());
			    if(arr[i]>max){
				    max=arr[i];
			    }
		    }
		    min=arr[0];
		    for(int i=0;i<arr.length; i++){
			    if(arr[i]<min){
				    min=arr[i];
			    }
		    }
		    System.out.println("The Maximun number is: "+max+" and Minimum is: "+min);
	    }
    }











