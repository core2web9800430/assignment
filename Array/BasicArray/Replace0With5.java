/*
 * You are given an integer N. You need to convert all zeros of N to 5.
 * Example 1:
 * Input:
 * N = 1004
 * Output: 1554
 * Explanation: There are two zeroes in 1004
 * on replacing all zeroes with "5", the new
 * number will be "1554".
 */

   import java.io.*;
   class Replace0With5 {
	   public static void main(String [] args)throws IOException{
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println("Enter the Number to be convert:");
		   int n  = Integer.parseInt(br.readLine());
		   int n2=n;
		   int count=0;
		   while(n>0){
			   count++;
			  n=n/10;
		   }
		   int arr[] = new int[count];
		   for(int i=0; i<count; i++){
			   int digit = n2%10;
			   if(digit==0){
				   digit=5;
			   }
			   arr[i]=digit;
			   n2=n2/10;
		   }
		   for(int i=count-1; i>=0; i--){
			   System.out.print(arr[i]);
		   }
		   System.out.println();
	   }
   }



