 /*
  * Given an array A[] of size n. The task is to find the largest element in it.
  * Example 1:
  * Input:
  * n = 5
  * A[] = {1, 8, 7, 56, 90}
  * Output: 90
  * Explanation:
  * The largest element of a given array is 90.
  */
  
  import java.io.*;
  class LargestElement {
	  public static void main(String [] args) throws IOException{
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of an Array:");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		System.out.println("Enter the Elements in array:");
		int larg=0;
		for(int i=0; i<arr.length; i++){
			arr[i]=Integer.parseInt(br.readLine());
			if(arr[i]>larg){
				larg=arr[i];
			}
		}
		System.out.println("The Largest Element in Array is: "+larg);
	  }
  }


	        	
