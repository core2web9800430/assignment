/*Given an array arr[] of non-negative integers and an integer sum, find a subarray
 * that adds to a given sum.
 * Note: There may be more than one subarray with sum as the given sum, print first
 * such subarray.
 * Examples:
 * Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
 * Output: Sum found between indexes 2 and 4
 * Explanation: Sum of elements between indices 2 and 4 is 20 + 3 + 10 = 33
 * Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
 * Output: Sum found between indexes 1 and 4
 * Explanation: Sum of elements between indices 1 and 4 is 4 + 0 + 0 + 3 = 7
 * Input: arr[] = {1, 4}, sum = 0
 * Output: No subarray found
 * Explanation: There is no subarray with 0 sum
 */
 
  import java.util.*;
  class FindSubArray {
	  public static void main(String [] args){
		  Scanner sc = new Scanner(System.in);
		  System.out.println("Enter the size of an array:");
		  int n= sc.nextInt();
		  System.out.println("Enter the elements in array:");
		  int arr[] = new int[n];
		  for(int i=0;i<n;i++){
			  arr[i]=sc.nextInt();
		  } boolean check=true;
		  System.out.println("Enter the sum:");
		  int sum = sc.nextInt();
		  for(int i=0; i<n; i++){
			 int sum1=arr[i] ;
			  if(sum1==sum){
				  System.out.println("Sum found index"+i);
			  }else{
				  for(int j= i+1; j<n; j++){
					  sum1+=arr[j];
					  if(sum1==sum){
						  check=false;
						  System.out.println("Sum found between index "+i+" and "+j);
					  } 
				  }
			  }
		  }
		  if(check){
			  System.out.println("There is no subarray with "+sum+" sum");
		  }
	  }
  }	  
