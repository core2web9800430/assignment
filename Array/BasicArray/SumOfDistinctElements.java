  /*You are given an array Arr of size N. Find the sum of distinct elements in an array.
   Example 1:
Input:N = 5
Arr[] = {1, 2, 3, 4, 5}
Output: 15
Explanation: Distinct elements are 1, 2, 3
4, 5. So the sum is 15.
Example 2:
Input:N = 5
Arr[] = {5, 5, 5, 5, 5}
Output: 5
Explanation: Only Distinct element is 5.
So the sum is 5.*/

   import java.util.*;
   class SumOfDistinctElements {
	   public static void main(String [] args){
		   Scanner sc = new Scanner(System.in);
		   System.out.println("Enter the size of array:");
		   int n = sc.nextInt();
		   int arr [] = new int[n];
		   System.out.println(" Enter the elements in Array:");
		   for(int i=0; i<n; i++){
			   arr[i]=sc.nextInt();
		   } 
		   int sum =0;
		   for (int i=0; i<n; i++){ 
			   boolean check = true;
			    for(int j=0;j<i; j++){
				    if(arr[i]==arr[j]){
					    check = false;
					    break;
				    } 
			    } 
			    if(check){
				    sum+=arr[i];
			    } 
		   }
		   System.out.println("sum of Distinct elements is : "+sum);
	   }
   
     }

