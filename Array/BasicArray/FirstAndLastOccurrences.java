/*
 *Given a sorted array having N elements, find the indices of the first and last
 occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.
Example 1:
Input:
N = 4 , X = 3
arr[] = { 1, 3, 3, 4 }
Output:
1 2
Explanation:
For the above array, first occurance of X = 3 is at index = 1 and last
occurrence is at index = 2
 */
  import java.util.*;  
  class FirstAndLastOccurrences {
	  public static void main(String [] args){
	      Scanner sc = new Scanner(System.in);
	      System.out.println("Enter the Size of an array:");
	      int n = sc.nextInt();
	      System.out.println("Enter the Elements in Array");
	      int arr[] = new int [n];
	      boolean bol =true;
	      for(int i=0; i<n; i++){
		      arr[i]=sc.nextInt();
	      }
	      System.out.println("Enter Element to check Occurance:");
	      int x= sc.nextInt();
	      for(int i=0; i<n; i++){
		      if(arr[i]== x){
			      System.out.print(i+" ");
		      bol= false;
		      } 
	      }
	      if(bol){
		       System.out.println("-1 \'Element is not Present\'"); 
	  }
     } 
  }

