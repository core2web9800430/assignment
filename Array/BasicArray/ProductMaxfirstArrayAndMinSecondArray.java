/*
 *Given two arrays of A and B respectively of sizes N1 and N2, the task is to
 calculate the product of the maximum element of the first array and minimum
 element of the second array.
 Example 1:
Input : A[] = {5, 7, 9, 3, 6, 2},
B[] = {1, 2, 6, -1, 0, 9}
Output : -9
Explanation:
The first array is 5 7 9 3 6 2.
The max element among these elements is 9.
The second array is 1 2 6 -1 0 9.
The min element among these elements is -1.
The product of 9 and -1 is 9*-1=-9.
Example 2:
Input : A[] = {0, 0, 0, 0},
B[] = {1, -1, 2}
Output : 0
 */
 
  import java.util.*;
  class ProductMaxfirstArrayAndMinSecondArray { 
	  public static void main(String [] args){
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter the Size of First Array:");
    int n1 = sc.nextInt();
    int arr[] = new int[n1];
    int max = 0;
    int min = 0;
    System.out.println("Enter the First Array Elements:");
    for (int i=0; i<n1; i++){
	    arr[i]=sc.nextInt();
    }
     System.out.println("Enter the Size of Second Array:");                                                                                                                     int n2 = sc.nextInt();                                                                                                                                                    int arr2[] = new int[n2];                                                                                                                                                  System.out.println("Enter the Second Array Elements:");                                                                                                                    for (int i=0; i<n2; i++){                                                                                                                                                         arr2[i]=sc.nextInt();
     }
    Arrays.sort(arr);
    Arrays.sort(arr2);
   int  mult = arr[n1-1]*arr2[0]; 
      System.out.println(" The Product of Maximum Number of First Array And Minimum  Number of Secound is: "+mult);
  }
  }

