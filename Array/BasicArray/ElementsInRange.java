/*
 *Given an array arr[] containing positive elements. A and B are two numbers
 defining a range. The task is to check if the array contains all elements in the given
 range.
 Example 1:
Input: N = 7, A = 2, B = 5
arr[] = {1, 4, 5, 2, 7, 8, 3}
Output: Yes
Explanation: It has elements between range 2-5 i.e 2,3,4,5.
 */
   
   import java.io.*;
   import java.util.*;
   class  ElementsInRange {
	   public static void main(String [] args)throws IOException{
		   ArrayList al =new ArrayList();
		   BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		   System.out.println("Enter the size of An Array:");
		   int size = Integer.parseInt(br.readLine());
		   int arr[] = new int[size];
		   System.out.println("Enter the Elements in array:");
		   for(int i=0; i<arr.length; i++){
			   arr[i] =Integer.parseInt(br.readLine());
		   }
		   System.out.println("Enter the first Number of range:");
		   int A = Integer.parseInt(br.readLine());
		   System.out.println("Enter the second Number:");
		   int B = Integer.parseInt(br.readLine());
		   for(int i=0; i<arr.length; i++){
			   if(arr[i]>=A && arr[i]<=B){
				   al.add(arr[i]);
			   }
		   }
		   ArrayList<Integer> al2 =new ArrayList();
		   for(int i=A; i<=B; i++){
			   al2.add(i);
		   } 
		   Collections.sort(al);
		   Collections.sort(al2); 
		   if (al.equals(al2)) {
		   System.out.println("Yes, the Elements in the range " + A + "-" + B + " are present: " + al);
		   } else {
		   System.out.print("No,In the range " + A + "-" + B + " Messing Elements are: ");
		   for (Integer i : al2) {
			    if (!al.contains(i)) {
				   System.out.print(i + " ");
			    }
		        }
		   } 
		   System.out.println();
	   }
   }
														               
											           




