/*Given an unsorted array Arr[] of N integers and an integer X, find floor and ceiling
 * of X in Arr[0..N-1].
 * Floor of X is the largest element which is smaller than or equal to X. Floor of X
 * doesn’t exist if X is smaller than the smallest element of Arr[].
 * Ceil of X is the smallest element which is greater than or equal to X. Ceil of X
 * doesn’t exist if X is greater than the greatest element of Arr[].
 * Example 1:
 * Input:N = 8, X = 7
 * Arr[] = {5, 6, 8, 9, 6, 5, 5, 6}
 * Output: 6 8
 * Explanation:
 * Floor of 7 is 6 and ceil of 7 is 8.
 * Example 2:
 * Input:N = 8, X = 10
 * Arr[] = {5, 6, 8, 9, 6, 5, 5, 6}
 * Output: 9 -1
 * Explanation:
 * Floor of 10 is 9 but ceil of 10 is not possible
 */

import java.util.*;
class CeilTheFloor {
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=sc.nextInt();
		System.out.println("Enter the Elements of Array:");
		int arr[] = new int[n];
		for(int i=0; i<n; i++){
			arr[i] = sc.nextInt();
		}
		System.out.println(" Enter the value of x:");
		int x =sc.nextInt();
		int floor =-1;
		int ceil =-1;
		Arrays.sort(arr);
		int max=arr[n-1];
		int min = arr[0];
		boolean check= true;
		for(int i=0; i<n; i++){
			if(arr[i]<=x && arr[i]>min){
				floor=arr[i];
			}
			if(arr[i]>=x && arr[i]<max){
			ceil=arr[i];
			} 
		} 
		if(floor==-1){
			check=false;
			 System.out.println("The Floor of "+x+" is not Possible in array and  ceil of "+x+ " is "+ceil); 
		} 
		if(ceil==-1){ 
			check = false;
			 System.out.println("The Floor of "+x+" is "+floor+" and  ceil of "+x+ " is not Possible in array "); 
		} 
		if(check){
	        System.out.println("The Floor of "+x+" is "+floor+" and  ceil of "+x+ " is "+ceil);	
		}
	}
}


