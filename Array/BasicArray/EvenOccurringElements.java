/*
 *Given an array Arr of N integers that contains an odd number of occurrences for all
 numbers except for a few elements which are present even number of times. Find
 the elements which have even occurrences in the array.
 Example 1:
Input:
N = 11
Arr[] = {9, 12, 23, 10, 12, 12,
15, 23, 14, 12, 15}
Output: 12 15 23
Example 2:
Input:
N = 5
Arr[] = {23, 12, 56, 34, 32}
Output: -1
Explanation:
Every integer is present odd number of times.
 */
  
 import java.util.*;
 class EvenOccurringElements { 
	 public static void main(String[] args){
		 Scanner sc= new Scanner(System.in);
		 ArrayList al=new ArrayList();
		 System.out.println("Enter the size of Array:");
		 int size = sc.nextInt();
		 int arr[] = new int[size];
		 int count2=0;
		 System.out.println("Enter the Elements in Array:");
		 for(int i=0; i<arr.length; i++){
			 arr[i]=sc.nextInt();
		 } 
		 for(int i=0; i<arr.length; i++){
			 int count=0;
			 for(int j=0; j<arr.length; j++){
				 if(arr[i]==arr[j]){
					 count++;
				 }
			 } 
				 if(count%2==0 && !al.contains(arr[i])){
					 al.add(arr[i]);
				  } else {
					  count2++;
				  }
		 }
		 if(count2==size){ 
			 System.out.println("-1 \"all numbers have odd occurrences in array\"");
		 } else {
			  System.out.print(al+"\"this numbers have even occurrences in array\" ");
	 }
     }
 }


   
