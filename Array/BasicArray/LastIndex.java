/*Given a string S consisting only '0's and '1's, find the last index of the '1' present in
 * it.
 * Example 1:
 * Input:
 * S = 00001
 * Output:
 * 4
 * Explanation:
 * Last index of 1 in the given string is 4.
 * Example 2:
 * Input:
 * 0
 * Output:
 * -1
 *  Explanation:
 *  Since, 1 is not present, so output is -1.
 */
   
   import java.util.*;
   class LastIndex {
	   public static void main(String[] args){
		   Scanner sc = new Scanner(System.in);
		   System.out.println("Enter the String:");
		   String s=sc.next();
		   sc.nextLine();
		   System.out.println("Enter char to search:");
		   String ks =sc.nextLine();
		   char k = ks.charAt(0);
		   int n= s.length();
		   int result=-1;
		   for(int i=0; i<n; i++){
			   if (s.charAt(i)== k ){
				   result=i;
			   }
		   }
		   System.out.println(" The Last Index of " +k+" is: "+result);
	   }
   }


