/*
 *
 */
    import java.io.*;
    class ProductOfElements {
	    public static void main(String [] args) throws IOException{
		    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		    System.out.println("Enter the size of array:");
		    int size = Integer.parseInt(br.readLine());
		    int arr[]= new int[size];
		    System.out.println("Enter the Element in Array:");
		    for(int i=0; i<arr.length; i++){
			    arr[i]=Integer.parseInt(br.readLine());
		    }
		    int mult=1;
		    for(int i=0; i<arr.length; i++){
			    mult*=arr[i];
		    }
		    System.out.println("The Product of all elements in array is: "+mult);
	    }
    }

