/*
 * Given an array of a fixed length. The task is to remove an element at a specific
 * index from the array.
 * Examples 1:
 * Input: arr[] = { 1, 2, 3, 4, 5 }, index = 2
 * Output: arr[] = { 1, 2, 4, 5 } 
 */
   
   import java.util.*;
   class RemoveIndexElement {
            public static void main(String [] args){
		    Scanner sc = new Scanner(System.in);
		    System.out.println("Enter the size of an Array");
		    int size = sc.nextInt();
		    int arr[] =new int[size];
		    System.out.println("Enter the Elements in Array:");
		    for(int i=0; i<size; i++){
			    arr[i] = sc.nextInt();
		    }
		    System.out.println("Enter the Index Number for Remove Element:");
		    int rmo = sc.nextInt();
		    if(rmo<0 || rmo>size){
			    System.out.println("Invalid Index");
		    } else {
		    for(int i=0; i<size; i++){
			    if (i==rmo){
			continue;
			    } else {
				    System.out.print(arr[i]+" ");
            		    } 
	               }
		    }
	    }
   }


