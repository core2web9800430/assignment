/*Given an array Arr of size N with all elements greater than or equal to zero. 
 * Return the maximum product of two numbers possible.
 * Example 1:
 * Input: N = 6
 * Arr[] = {1, 4, 3, 6, 7, 0}
 * Output: 42
 * Example 2:
 * Input:N = 5
 * Arr = {1, 100, 42, 4, 23}
 * Output: 4200
 */
import java.util.*;
class MaxProductOfTwoNumbers{
	public static void main(String [] args){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of array:");
		int n=sc.nextInt();
		System.out.println("Enter the Elements of Array:");
		int arr[] = new int[n];
		for(int i=0; i<n; i++){
			arr[i] = sc.nextInt();
		} 
		int max = 0;
		for(int i=0; i<n; i++){
                       int mult= 1;
		       for(int j=i+1; j<n; j++){
			       mult=arr[i]*arr[j];
			        if(mult>max){ 
					 max = mult;
				}
		       } 
		} 
		if(max==0){
			System.out.println("No valid pair found");
		} else {
	        System.out.println("The maximum Product of two elements in Array is: "+max);
	        }	
	}
 }



