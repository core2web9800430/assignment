/*
 *Given an array of size n which contains all elements occurring in multiples of K,
 except one element which doesn't occur in multiple of K. Find that unique element.
 Example 1:
Input :
n = 7, k = 3
arr[] = {6, 2, 5, 2, 2, 6, 6}
Output :
5
Explanation:
Every element appears 3 times except 5.
Example 2:
Input :
n = 5, k = 4
arr[] = {2, 2, 2, 10, 2}
Output :
10
Explanation:
Every element appears 4 times except 10.
 */

  import java.util.*;
  class UniqueElement {
	  public static void main(String [] args){
		  Scanner sc= new Scanner(System.in);
		  System.out.println("Enter the Size of array:");
		  int n = sc.nextInt();
		  System.out.println("Enter the elements in array:");
		  int arr[] = new int[n];
		  for (int i=0; i<n; i++){
			  arr[i] = sc.nextInt();
		  } System.out.println("Enter the k multiple number:");
		  int k=sc.nextInt();
		  for (int i=0; i<n; i++){
			  int count=0;
			  for (int j=0; j<n; j++){
				  if(arr[i] ==arr[j]){
					  count++;
				  }
			  }
			  if(count!=k){
				  System.out.println( " Unique Number is: "+arr[i]);
			  }

		  }
	  }
  }



