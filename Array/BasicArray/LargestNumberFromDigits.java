/*
 *Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
 of the array such that after combining all the elements of the array, the number
 formed is maximum.
 Example 1:
 Input:
 N = 5
 A[] = {9, 0, 1, 3, 0}
 Output:
 93100
 Explanation:
 Largest number is 93100 which can be formed from array digits
 *
 */


   import java.util.*;
   class LargestNumberFromDigits {
	   public static void main(String [] args){
		   Scanner sc= new Scanner(System.in);
		   ArrayList al = new ArrayList();
		   System.out.println("Enter the size of array:");
		   int size = sc.nextInt();
		   System.out.println("Enter the elements in array");
		   for(int i=0; i<size; i++){ 
			   int digit =sc.nextInt();
			   al.add(digit);
		   }
		   Collections.sort(al, Collections.reverseOrder());
		    System.out.print(" Largest Number is: ");
		   for(int i=0;i<size;i++){
			   System.out.print(al.get(i));
	   }
	   System.out.println();
   }
   }


		   



