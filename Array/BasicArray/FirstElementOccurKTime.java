/*Given an array of N integers. Find the first element that occurs at least K number
 * of times.
 * Example 1:
 * Input :
 * N = 7, K = 2
 * A[] = {1, 7, 4, 3, 4, 8, 7}
 * Output :
 * 4
 * Explanation:
 * Both 7 and 4 occur 2 times.
 * But 4 is first that occurs 2 times
 * As at index = 4, 4 has occurred
 * at least 2 times whereas at index = 6,
 * 7 has occurred at least 2 times.
 */
import java.util.*;
class FirstElementOccurKTime {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an Array:");
		int n = sc.nextInt();
		System.out.println("Enter the elements in Array:");
		int arr[] = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the k number:");
		int k = sc.nextInt();
		for (int i=0; i<n; i++){
			int count=1;
			for(int j=i+1; j<n; j++){
				if(arr[i] == arr[j]){ 
					count++;
				}
			} 
			if (count == k) {
			     System.out.println("The first element that occurs at least " + k + " times is: " + arr[i]);
						        break;
		} 	       
	}
}
}    
