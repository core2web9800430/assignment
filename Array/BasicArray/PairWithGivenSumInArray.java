/*Given an array A[] of n numbers and another number x, the task is to check
 * whether or not there exist two elements in A[] whose sum is exactly x.
 * Examples:
 * Input: arr[] = {0, -1, 2, -3, 1}, x= -2
 * Output: Yes
 * Explanation: If we calculate the sum of the output,1 + (-3) = -2
 * Input: arr[] = {1, -2, 1, 0, 5}, x = 0
 * Output: No
 */

import java.util.*;
class PairWithGivenSumInArray {
	public static void main(String [] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of an Array:");
		int n = sc.nextInt();
		System.out.println("Enter the elements in Array:");
		int arr[] = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the sum:");
		int sum = sc.nextInt();
		int sum2=0;
		boolean check = true;
		for (int i=0; i<n; i++){
			for(int j=i+1; j<n; j++){
				sum2=arr[i]+arr[j];
				if(sum2 == sum){
					check = false;
					System.out.println("Yes");
				}
			}
		} 
	       if(check){
	       System.out.println("NO");
	       }	       
	}
}
    
