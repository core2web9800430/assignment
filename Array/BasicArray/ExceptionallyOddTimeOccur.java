/*Given an array of N positive integers where all numbers occur even number of
 * times except one number which occurs odd number of times. Find the exceptional
 * number.
 * Example 1:
 * Input:N = 7
 * Arr[] = {1, 2, 3, 2, 3, 1, 3}
 * Output: 3
 * Explanation: 3 occurs three times.
 * Example 2:
 * Input:N = 7
 * Arr[] = {5, 7, 2, 7, 5, 2, 5}
 * Output: 5
 * Explanation: 5 occurs three times.
 */
import java.util.*;
class  ExceptionallyOddTimeOccur {
	public static void main(String [] args){
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter the size of array:");
		int n= sc.nextInt();
		System.out.println("Enter the Array Elements:");
		int arr[] = new int[n];
		for(int i=0; i<n; i++){
			arr[i]=sc.nextInt();
		} 
		int count;
		for(int i=0; i<n; i++){
			count=0;
			for(int j=0; j<n; j++){
				if(arr[i]==arr[j]){
					count++;
				}
			}
		       	if(count%2!=0){
				System.out.println(" In Array the odd Occurance number is: "+ arr[i]);
				break;
			}
		}
	}
}




