 /*
  *Given an array Arr of size N, the array contains numbers in range from 0 to K-1
  where K is a positive integer and K <= N. Find the maximum repeating number in
  this array. If there are two or more maximum repeating numbers return the element
  having least value.
  Example 1:
Input:N = 4, K = 3
Arr[] = {2, 2, 1, 2}
Output: 2
Explanation: 2 is the most frequent element.
Example 2:
Input:N = 6, K = 3
Arr[] = {2, 2, 1, 0, 0, 1}
Output: 0
Explanation: 0, 1 and 2 all have the same frequency of 2.But since 0 is
smallest, you need to return 0.
  */

  import java.util.*;
  class MaximumRepeatingNumber {
	  public static void main(String [] args){
		  Scanner sc = new Scanner(System.in);
		  System.out.println("Enter the Size of an array:");
		  int size = sc.nextInt();
		  int arr[] = new int[size];
		  System.out.println("Enter the k where k is positive integer:");
		  int k= sc.nextInt();
		  System.out.println("Enter the Elements in Array in range from 0 to k-1:");
		  for (int i=0; i<size; i++){
			  arr[i]= sc.nextInt();
		  } 
		 int arr2[] = new int [size]; 
		  int maxcount=0;
		  for(int i=0; i<size; i++){
			  int count=0;
			  for(int j=0; j<size; j++){
				  if(arr[i]==arr[j]){
					  count++;
				  }
			  } 
			  arr2[i] = count;
				  if(count>maxcount){
					  maxcount=count;
				   } 
		  } 
		  int count2=0;
		  int result=0;
		  for(int i=0;i<size; i++){
			  if(arr2[i]==maxcount){ 
				  count2++;
				  result = arr[i];
			  }
		  } 
		  if (count2>1){ 
			  Arrays.sort(arr);
			   System.out.print("the most reapeating element is : "+arr[0]); 
		  } else {
			   System.out.print("the most reapeating element is : "+ result); 
	  }
  }
  }



