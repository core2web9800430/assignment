/*Given an array of integers, your task is to find the smallest and second smallest
 * element in the array. If smallest and second smallest do not exist, print -1.
 * Example 1:
 * Input :n=5
 * 2 4 3 5 6
 * Output : 2 3
 * Explanation:
 * 2 and 3 are respectively the smallest
 * and second smallest elements in the array.
 * Example 2:
 * Input :n=6
 * 1 2 1 3 6 7
 * Output : 1 2
 * Explanation:
 * 1 and 2 are respectively the smallest
 * and second smallest elements in the array.
 */
 import java.util.*;
 class  SmallestAndSecondSmallestElement {
	 public static void main(String [] args){
		 Scanner sc = new Scanner (System.in);
		 System.out.println("Enter the Size of array:");
		 int n= sc.nextInt();
		 System.out.println("Enter the Elements in array:");
		 int arr[]= new int[n];
		 for (int i=0; i<n; i++){
			 arr[i]=sc.nextInt();
		 }
		 int min =arr[0];
		 int Smin=arr[1];
		 for( int i=0; i<n; i++){
			 if(arr[i]<min){
				 min=arr[i];
			 } 
			 if(arr[i]<Smin && arr[i]>min){
				Smin=arr[i];
			 }
		 }
	System.out.println(" Smallest Element is "+min+" and Second Smallest Number is "+ Smin);
	 }
 }


