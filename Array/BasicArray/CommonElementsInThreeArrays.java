/*Given three Sorted arrays in non-decreasing order, print all common elements in
 * these arrays.
 * Examples:
 * Input:
 * ar1[] = {1, 5, 10, 20, 40, 80}
 * ar2[] = {6, 7, 20, 80, 100}
 * ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
 * Output: 20, 80
 * Input:
 * ar1[] = {1, 5, 5}
 * ar2[] = {3, 4, 5, 5, 10}
 * ar3[] = {5, 5, 10, 20}
 * Output: 5, 5
 */ 

import java.util.*;
 class CommonElementsInThreeArrays { 
	 public static void main(String[] args){
		 Scanner sc= new Scanner(System.in);
		 System.out.println("Enter the size of first Array:");
		 int n1= sc.nextInt();
		 System.out.println("Enter the Elements of first Array:");
		 int arr1[]=new int[n1];
		 for(int i=0; i<n1; i++){
			 arr1[i]=sc.nextInt();
		 } 
		 System.out.println("Enter the size of Second Array:");
		  int n2= sc.nextInt();
		   System.out.println("Enter the Elements of Second Array:");
		   int arr2[]=new int[n2];
		  for(int i=0; i<n2; i++){
			   arr2[i]=sc.nextInt();
		  } 
	         System.out.println("Enter the size of Third Array:");
		int n3= sc.nextInt();
		System.out.println("Enter the Elements of Third Array:");
                int arr3[]=new int[n3];
		for(int i=0; i<n3; i++){
			 arr3[i]=sc.nextInt();
		}
		for(int i=0; i<n1; i++){
			for(int j=0; j<n2; j++){
				for(int k=0; k<n3; k++){
					if (arr1[i]==arr2[j] && arr2[i]==arr3[k]){
						System.out.print(arr1[i]+" ");
					}
				}
			}
		}
	 }
 }

